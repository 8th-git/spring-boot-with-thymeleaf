package com.hrdcenter.springbootthymeleaf.controller;

import com.hrdcenter.springbootthymeleaf.model.User;
import com.hrdcenter.springbootthymeleaf.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/admin/user/add")
    public String addUser(@ModelAttribute @Valid User user, BindingResult result) {

        if (result.hasErrors()) {
            return "/admin/user/user-add";
        }

        user.setUuid(UUID.randomUUID().toString());

        // invoke save user
        userRepository.save(user);

        return "redirect:/admin/user";
    }

    @GetMapping("/admin/user/add")
    public String viewAddUserPage(@ModelAttribute User user,
                                  ModelMap modelMap) {

        modelMap.addAttribute("user", user);

        return "/admin/user/user-add";
    }

    @GetMapping("/admin/user")
    public String viewUserPage(ModelMap modelMap, @RequestParam(required = false) String lang,
                               @RequestParam(required = false) String query) {

        List<User> users = (List<User>) userRepository.findAll();

        modelMap.addAttribute("users", users);

        return "/admin/user/user-list";
    }
}
